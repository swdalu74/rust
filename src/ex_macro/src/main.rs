#![allow(unused)]
fn main() {
    // 표현식으로 사용됩니다.
    let x = vec![1,2,3];

    // 문으로 사용됩니다.
    println!("Hello!");

    // 패턴에 사용됩니다.
    macro_rules! pat {
        ($i:ident) => (Some($i))
    }

    if let pat!(x) = Some(1) {
        assert_eq!(x, 1);
    }

    // 유형에 사용됩니다.
    macro_rules! Tuple {
        { $A:ty, $B:ty } => { ($A, $B) };
    }

    type N2 = Tuple!(i32, i32);

    let nn: N2 = (1, 1);

    println!("{:?}", nn);

    // 항목으로 사용됩니다.
    use std::cell::RefCell;
    thread_local!(static FOO: RefCell<u32> = RefCell::new(1));

    // 관련 항목으로 사용됩니다.
    macro_rules! const_maker {
        ($t:ty, $v:tt) => { const CONST: $t = $v; };
    }
    trait T {
        const_maker!{i32, 7}
    }

    // 매크로 내에서 매크로 호출.
    macro_rules! example {
        () => { println!("Macro call in a macro!") };
    }
    // 외부 매크로`example`이 확장되고 내부 매크로`println`이 확장됩니다.
    example!();
}