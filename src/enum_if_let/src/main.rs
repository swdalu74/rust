fn main() {
    let a = Some(3);
    let b = 3;
    if let Some(3) = a {
        println!("three");
    }

    if let None = a {
        println!("none");
    }

    test_if_let(a);
    test_if_let(None);
}

fn test_if_let(o: Option<i32>) {
    if let Some(value) = o {
        println!("Option<i32>::Some({})", value);
    } else {
        //None
        println!("Optoin<i32>::None");
    }
}