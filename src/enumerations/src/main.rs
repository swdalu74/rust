#![allow(unused)]
fn main() {
    #[derive(Debug)]
    enum Message {
        Quit(),
        Move { x: i32, y: i32 },
        Write(String),
        ChangeColor(i32, i32, i32),
    };

    impl Message {
        fn call(&self) {
            // 메소드 내용은 여기 정의할 수 있습니다.
            match *self {
                /*
                Message::Quit => {
                    println!("Quit");
                },
                Message::Move {x, y} => {
                    println!("Move: x {}, y {}", x, y);
                },
                */
                Message::Write(ref msg) => {
                    println!("Write: {}", msg);
                },
                /*
                Message::ChangeColor(x, y, z) => {
                    println!("ChangeColor: {}, {}, {}", x, y, z);
                },
                */
                _ => {
                    println!("{:?}", self);
                }
            }
        }
    }

    let m = Message::Write(String::from("hello"));
    m.call();
    let m2 = Message::Quit();
    m2.call();
}