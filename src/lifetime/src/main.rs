fn main() {
    let r;

    {
        let mut x = 5;
        r = &mut x;
        changev(r);
        println!("x : {}", x);
    }
    println!("r: {}", r);
}

fn changev(r: &mut i32) {
    *r = 10;
}


