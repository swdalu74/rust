/*
    주석됨?
*/
fn five() -> i32 {
    5
}

fn five_with_return() -> i32 {
    return 5;
}

fn main() {
    let x = five();
    println!("The value of x is: {}", x);
    let y = five_with_return();
    println!("The value of y is: {}", y);
}

