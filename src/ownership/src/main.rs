fn main() {
    let s = String::from("hello");
    takes_ownership(s);

    //error: reference s after move
    //println!("after move : {0}", s);

    let x = 5;
    makes_copy(x);
    println!("after copy : {}", x);

    let s2 = String::from("reference");
    let len = calculate_length(&s2);
    println!("The length of '{}' is {}.", s2, len);

    let mut s3 = String::from("hello");
    change(&mut s3);
    println!("{}", s3);


    let mut s4 = String::from("hello");

    let r1 = &mut s4;
    let r2 = &mut s4;
    let len = calculate_length_with_ref(r1);
    println!("The length of '{}' is {}.", r1, len);
}

fn takes_ownership(some_string: String) {
    println!("{}", some_string);
}

fn makes_copy(some_integer: i32) {
    println!("{}", some_integer);
}

fn calculate_length(s: &String) -> usize {
    s.len()
}

fn calculate_length_with_ref(s: &mut String) -> usize {
    s.len()
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}

// 빌린 값을 수정 시도
/*
fn change(some_string: &String) {
    some_string.push_str(", world");
}
*/

// 댕글링 참조자(Dangling References)
/*
fn dangle() -> &String {
    let s = String::from("hello");

    &s
}
*/