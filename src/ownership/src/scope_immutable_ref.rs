fn main() {
    let mut s = String::from("hello");
    let r1 = &mut s; // 문제 없음
    let r2 = &mut s; // 문제 없음
    println!("r1 is '{}'", r1);
    println!("r2 is '{}'", r2);
}