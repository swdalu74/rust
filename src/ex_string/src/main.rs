fn main() {
    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");

    let s = format!("{}-{}-{}", s1, s2, s3);

    println!("{}", s);
    println!("{}", s1);
    println!("{}", s2);
    println!("{}", s3);
    {
        let an = '안';
        println!("length : {:?}",an.len_utf8());
        let hello = "안녕하세요";
        let answer = &hello[0..2];
        println!("{:?}", answer);
        println!("{}", hello.len()/hello.chars().count());

        for c in hello.chars() {
            println!("{:?}", c);
        }
    }
}
