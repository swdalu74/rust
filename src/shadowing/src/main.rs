fn main() {
    let x = 5;

    let x = x + 1;

    let x = x * 2;

    println!("The value of x is: {}", x);

    let spaces = "          ";
    let spaces = spaces.len();
    println!("spaces len is {}", spaces);

    //mut을 사용하는 경우 mismatched types error가 발생함.
    // let mut spaces = "   ";
    // spaces = spaces.len();
    // println!("spaces len is {}", spaces);
}