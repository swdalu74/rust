fn main() {
    struct Color(i32, i32, i32);
    struct Point(i32, i32, i32);
    struct MyTupleStruct(i32, String);

    let black = Color(0, 1, 2);
    let origin = Point(0, 3, 4);
    let user = MyTupleStruct(45, String::from("Indal"));

    println!("{:?}", user.1);

    let MyTupleStruct(age, name) = user;
    println!("age : {}, name : {}", age, name);

    let tup: (i32, &str) = (500, "my name");

    let (age, name) = tup;
    println!("age : {}, name : {}", age, name);
}