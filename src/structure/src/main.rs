fn main() {
    #[derive(Debug)]
    struct User {
        username: String,
        email: String,
        sign_in_count: u64,
        active: bool,
    }

    #[derive(Debug, Clone, Copy)]
    struct UserWithoutString {
        id: i64,
        age: i32,
        active: bool
    }

    let user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };

    let user2 = User {
        email: String::from("another@example.com"),
        username: String::from("anotherusername567"),
        ..user1
    };

    let user4 = UserWithoutString {
        id: 0xffff,
        age: 20,
        active: true
    };

    let user5 = user4;
    println!("{:?}", user4);
    println!("{:?}", user5);

    // let user3 = User {
    //     ..user1
    // };
    println!("{:p}", &user2);
    let user3 = &user2;
    println!("{:p}", &user3);
    //println!("{:?}", user2);
    println!("{:?}", user3);
}
