use std::fs::File;
use std::io::ErrorKind;
use std::any::type_name;

fn type_of<T>(_: T) -> &'static str {
    type_name::<T>()
}

fn main() {
    let f = File::open("hello.txt");

    let f = match f {
        Ok(ref file) => {
            println!("{}", type_of(&file));
            file
        },
        Err(error) if error.kind() == ErrorKind::NotFound => {
            println!("{}", type_of(error));
            match File::create("hello.txt") {
                Ok(fc) => fc,
                Err(e) => {
                    panic!(
                        "Tried to create file but there was a problem: {:?}",
                        e
                    )
                },
            }
        },
        Err(error) => {
            panic!(
                "There was a problem opening the file: {:?}",
                error
            )
        },
    };
}