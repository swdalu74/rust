fn main() {
    let v = vec![1, 2, 3];

    let third1: &i32 = &v[2];
    println!("(1) Third element is {}", third1);
    let third2: Option<&i32> = v.get(2);
    println!("(2) Third element is {:?}", third2);
    match v.get(2) {
        Some(third3) => println!("(3) Third element : {}", third3),
        None => println!("(3) Third element doesn't exist")
    }

    {
        let mut v = vec![1, 2, 3, 4, 5];
        for i in &mut v {
            *i += 50;
            println!("{}", i);
        }
    }
    {
        #[derive(Debug)]
        enum SpreadsheetCell {
            Int(i32),
            Float(f64),
            Text(String),
        }

        let row = vec![
            SpreadsheetCell::Int(3),
            SpreadsheetCell::Text(String::from("blue")),
            SpreadsheetCell::Float(10.12),
        ];

        println!("{:?}", row);

        for i in row {
            println!("{:?}", i);
        }
    }
}
