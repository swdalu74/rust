use std::any::type_name;
use std::sync::Once;

static ONCE: Once = Once::new();

fn type_of<T>(_: T) -> &'static str {
    type_name::<T>()
}

fn main() {
    use std::collections::HashMap;

    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    let team_name = String::from("Blue");
    let score = scores.get(&team_name);

    {
        let mut scores2 = HashMap::new();

        scores2.insert(String::from("Blue"), 10);
        scores2.insert(String::from("Yellow"), 20);

        for (key, value) in &scores2 {
            ONCE.call_once(|| {
                println!("key is {}", type_of(key));
                println!("value is {}", type_of(value));
            });
            println!("{} : {}", key, value);
        }
    }

    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Blue"), 25);

    println!("{:?}", scores);

    {
        let text = "hello world wonderful world";

        let mut map = HashMap::new();

        for word in text.split_whitespace() {
            println!("{:?}", map.entry(word));
            let count = map.entry(word).or_insert(0);
            *count += 1;
        }

        println!("{:?}", map);
    }
}
