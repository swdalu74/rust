#[derive(Debug)]
struct Rectangle {
    length: u32,
    width: u32,
}

// Method는 impl (구현: implementation) 블록 시작
impl Rectangle {
    // 첫번째 파라미터는 self의 타입이 Rectangle
    // &mut self도 가능
    fn area(&mut self) -> u32 {
        self.length = 10;
        self.length * self.width
    }
}

fn main() {
    let mut rect1 = Rectangle { length: 50, width: 30 };

    println!(
        "The area of the rectangle is {} square pixels.",
        rect1.area()
    );
}
