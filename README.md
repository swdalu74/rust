---
marp: true
paginate: false
backgroundColor: white
---

<style>
h1 {
  font-size:  1.9rem;
}
/*
h2 {
  color: #8b0000;
}
*/
section.lead h1 {
  text-align: center;
}

section.lead h2 {
  text-align: center;
}
</style>

<!--
theme: default
class: lead
paginate: true
-->

# _The Rust Programming Language_

---

## [설치하기](https://rinthel.github.io/rust-lang-book-ko/ch01-01-installation.html)

### [https://rinthel.github.io/rust-lang-book-ko/ch01-01-installation.html](https://rinthel.github.io/rust-lang-book-ko/ch01-01-installation.html)

---

# Hello, World!
<br>


```rust

fn main() {
    println!("Hello, world!");
}

```

```bash
$ rustc main.rs
$ ./main
Hello, world!
```

---

# Hello, Cargo! (1)

+ Cargo(카고) :  러스트의 빌드 시스템 및 패키지 매니저

+ Cargo를 사용하여 프로젝트 생성
    ```bash
    $ cargo new hello_cargo --bin
    $ cd hello_cargo
    ```

+ Cargo.toml : Cargo의 환경설정 포맷 (_TOML : Tom’s Obvious, Minimal Language_)
    ```TOML
    [package]
    name = "hello_cargo"
    version = "0.1.0"
    edition = "2018"
    ```
---

+ Cargo 프로젝트 빌드 및 실행

    ```bash
    $ cargo build
        Compiling hello_cargo v0.1.0 (file:///projects/hello_cargo)
         Finished dev [unoptimized + debuginfo] target(s) in 2.85 secs
    $ ./target/debug/hello_cargo
    Hello, world!
    $ cargo run
        Finished dev [unoptimized + debuginfo] target(s) in 0.0 secs
         Running `target/debug/hello_cargo`
    Hello, world!
    ```

+ Release build
    ```
    $ cargo build --release
    ```

---

# 보편적인 프로그래밍 개념

---

# 변수와 가변성

+ variables 라는 새 프로젝트를 생성
    ```bash
    $ cargo new --bin variables
    ```

+ Filename: src/main.rs
    ```rust
    fn main() {
        let x = 5;
        println!("The value of x is: {}", x);
        x = 6;
        println!("The value of x is: {}", x);
    }
    ```
---

+ cargo run 명령을 통해 실행

    ```bash
    $ cargo run
        Compiling variables v0.1.0 (src/variables)
    error[E0384]: cannot assign twice to immutable variable `x`
    --> src/main.rs:4:5
    |
    2 |     let x = 5;
    |         -
    |         |
    |         first assignment to `x`
    |         help: consider making this binding mutable: `mut x`
    3 |     println!("The value of x is: {}", x);
    4 |     x = 6;
    |     ^^^^^ cannot assign twice to immutable variable

    ```

    > compile error의 원인은 우리가 불변성 변수 x에 두 번째로 값을 할당했기 때문
    > let keyword : 새로운 변수의 선언.
    > Rust는 기본적인 변수 선언은 불변성 변수를 생성
---

+ 가변성 변수 선언

    ```rust
    fn main() {
        let mut x = 5;
        println!("The value of x is: {}", x);
        x = 6;
        println!("The value of x is: {}", x);
    }
    ```

    ```bash
    $ cargo run
        Compiling variables v0.1.0 (src/variables)
         Finished dev [unoptimized + debuginfo] target(s) in 0.30 secs
          Running `target/debug/variables`
    The value of x is: 5
    The value of x is: 6
    ```
---
+ 상수 (const) : mut을 사용하는 것이 허용되지 않음

    ```rust
    const MAX_POINTS: u32 = 100_000;
    ```

+ Shadowing : 이전에 선언한 변수와 같은 이름의 새 변수를 선언

    ```rust
    fn main() {
        let x = 5;

        let x = x + 1;

        let x = x * 2;

        println!("The value of x is: {}", x);
    }
    ```
    > 첫 변수가 두 번째에 의해 shadowed 됐다고 표현
    > 첫 번째 x는 두 번째 x에 의해서 shadowed됨.
    > 두 번째 x는 세 번째 x에 의해서 shadowed됨.

---
# Data types

![width:2048](data_types.png)

---

+ 부동 소수점 타입 : IEEE-754 표준에 따라 표현
    ```rust
    fn main() {
        let x = 2.0; // f64

        let y: f32 = 3.0; // f32
    }
    ```

+ 수학적 연산들
    ```rust
    fn main() {
        let sum = 5 + 10; // addition
        let difference = 95.5 - 4.3; // subtraction
        let product = 4 * 30; // multiplication
        let quotient = 56.7 / 32.2; // division
        let remainder = 43 % 5; // remainder
    }
    ```

---

+ Boolean 타입

    ```rust
    fn main() {
        let t = true;

        let f: bool = false; // with explicit type annotation
    }
    ```

+ 문자 타입

    ```rust
    fn main() {
    let c = 'z';
    let z = 'ℤ';
    let heart_eyed_cat = '😻';
    }
    ```

---

+ 복합 타입들 : 튜플과 배열

+ 튜플

    ```rust
    fn main() {
        let x: (i32, f64, u8) = (500, 6.4, 1);

        let five_hundred = x.0;

        let six_point_four = x.1;

        let one = x.2;
    }
    ```
    ```rust
    fn main() {
        let tup = (500, 6.4, 1);

        let (x, y, z) = tup;

        println!("The value of y is: {}", y);
    }
    ```

---
+ 배열

    ```rust
    fn main() {
        let a = [1, 2, 3, 4, 5];
        let months = ["January", "February", "March", "April", "May", "June", "July",
                    "August", "September", "October", "November", "December"];
    }
    ```
    ```rust
    fn main() {
        let a = [1, 2, 3, 4, 5];

        let first = a[0];
        let second = a[1];
    }
    ```
---
+ 유효하지 않은 배열 요소에 대한 접근

    ```rust
    fn main() {
        let a = [1, 2, 3, 4, 5];
        let index = 10;

        let element = a[index];

        println!("The value of element is: {}", element);
    }
    ```
    ```bash
    $ cargo run
        Compiling arrays v0.1.0 (file:///projects/arrays)
         Finished dev [unoptimized + debuginfo] target(s) in 0.31 secs
          Running `target/debug/arrays`
    thread '<main>' panicked at 'index out of bounds: the len is 5 but the index is
    10', src/main.rs:6
    note: Run with `RUST_BACKTRACE=1` for a backtrace.
    ```
---
# 함수 동작 원리

+ 함수의 구현

    ```rust
    fn main() {
        println!("Hello, world!");
        another_function();
    }

    fn another_function() {
        println!("Another function.");
    }
    ```
    > Rust에서 함수는 fn으로 시작하고, "snake case"를 변수나 함수 이름 규칙으로 사용
    > Rust는 당신의 함수의 위치와 상관없이 정의만 되어 있으면 호출 가능
---
+ 함수 매개변수

    ```rust
    fn main() {
        another_function(5);
    }

    fn another_function(x: i32) {
        println!("The value of x is: {}", x);
    }
    ```
    ```rust
    fn main() {
        another_function(5, 6);
    }

    fn another_function(x: i32, y: i32) {
        println!("The value of x is: {}", x);
        println!("The value of y is: {}", y);
    }
    ```
---
+ 구문과 표현식

    ```rust
    fn main() {
        let x = 5; // <- 구문
        let y = {  // <- 표현식
            let x = 3;
            x + 1 //<- return
        };

        println!("The value of y is: {}", y);
    }
    ```
+ 표현식

    ```rust
    {
        let x = 3;
        x + 1
    }
    ```
    > 이 표현식은 4를 산출
    > __표현식은 종결을 나타내는 세미콜론(;)을 사용하지 않음.__
---
+ 반환 값을 갖는 함수

    ```rust
    fn main() {
        let x = plus_one(5);

        println!("The value of x is: {}", x);
    }

    fn plus_one(x: i32) -> i32 {
        x + 1
    }
    ```
    > plus_one 함수의 x + 1에 세미콜론(;)이 없는 이유는 값을 반환하고자 할 때 사용하는 표현식이기 때문임.
---
+ 표현식을 구문으로 변경

    ```rust
    fn main() {
        let x = plus_one(5);
        println!("The value of x is: {}", x);
    }
    fn plus_one(x: i32) -> i32 {
        x + 1;
    }
    ```
    ```bash
    error[E0308]: mismatched types
    --> src/main.rs:7:28
    |
    7 |   fn plus_one(x: i32) -> i32 {
    |  ____________________________^
    8 | |     x + 1;
    | |          - help: consider removing this semicolon
    9 | | }
    | |_^ expected i32, found ()
    |
    = note: expected type `i32`
                found type `()`
    ```
---
# 제어문
+ if 표현식 : Rust는 boolean 타입이 아닌 것을 boolean 타입으로 자동 변환하지 않음

    ```rust
    fn main() {
        let number = 3;
        if number < 5 {
            println!("condition was true");
        } else {
            println!("condition was false");
        }
    }
    ```
    ```rust
    fn main() {
        let number = 3;
        if number { // <- error : expected bool, found integral variable
            println!("number was three");
        }
    }
    ```
---
+ else if와 다수 조건

    ```rust
    fn main() {
        let number = 6;

        if number % 4 == 0 {
            println!("number is divisible by 4");
        } else if number % 3 == 0 {
            println!("number is divisible by 3");
        } else if number % 2 == 0 {
            println!("number is divisible by 2");
        } else {
            println!("number is not divisible by 4, 3, or 2");
        }
    }
    ```
    > 너무 많은 else if식의 사용은 당신의 코드를 이해하기 어렵하기 때문에 match라 불리는 강력한 분기 생성자를 사용하는 것이 좋음
---
+ let구문에서 if 사용하기

    ```rust
    fn main() {
        let condition = true;
        let number = if condition {
            5
        } else {
            6
        };

        println!("The value of number is: {}", number);
    }
    ```
    > if 표현식을 사용하여 결과값을 변수에 대입할 수 있음
---
+ 잘못된 let 구문에서 if 사용

    ```rust
    fn main() {
        let condition = true;
        let number = if condition {
            5
        } else {
            "six"
        };
    }
    ```
    ```bash
    error[E0308]: if and else have incompatible types
    --> src/main.rs:4:18
    |
    4 |       let number = if condition {
    |  __________________^
    5 | |         5
    6 | |     } else {
    7 | |         "six"
    8 | |     };
    | |_____^ expected integral variable, found reference
    |
    ```
---
+ 반복문과 반복 : Rust가 제공하는 세 가지 반복문: loop, while, 그리고 for

+ loop와 함께 코드의 반복 수행

    ```rust
    fn main() {
        loop {
            println!("again!");
        }
    }
    ```
    > break keyword로 loop를 종료할 수 있음

+ while와 함께하는 조건부 반복
    ```rust
    fn main() {
        let mut number = 3;
        while number != 0 {
            number = number - 1;
        }
        println!("LIFTOFF!!!");
    }
    ```
---
+ for와 함께하는 콜렉션 반복하기

    ```rust
    fn main() {
        let a = [10, 20, 30, 40, 50];

        for element in a.iter() {
            println!("the value is: {}", element);
        }
    }
    ```
+ Rust에서 기본 라이브러리로 제공하는 Rang
    ```rust
    fn main() {
        for number in 1..4 {
            println!("{}!", number);
        }
        for number in (1..4).rev() {
            println!("{}!", number);
        }
    }
    ```
---
# 소유권 이해하기
## 소유권(Ownership) : Rust의 가장 유니크한 특성
---
+ __소유권이란? Rust의 핵심 기능__

    - 모든 프로그램은 실행하는 동안 메모리를 관리해야 함

    - Golang, Java, Kotlin, Pythonn 과 같은 언어는 __가비지 콜렉션__ 를 가지고 메모리를 관리

    - C/C++ 같은 저수준 언어는 __직접 명시적으로 메모리를 할당하고 해제__ 해야 함

    - Rust는 제 3의 접근법을 이용: 메모리는 컴파일 타임에 컴파일러가 체크할 규칙들로 구성된 __소유권 시스템__ 을 통해 관리

    - 소유권 기능들의 어떤 것도 __런타임 비용__ 이 발생하지 않음

    - 소유권이란 개념은 생소하여 이해하고 사용하는 어려움이 있지만 __더 안전하고 더 효율적인 코드__ 를 자연스럽게 개발할 수 있음
---
# Stack과 Heap
---
+ 소유권 규칙

    - 러스트의 각각의 값은 해당값의 __오너(owner)라고 불리우는 변수__ 를 갖고 있다.

    - __한번에 딱 하나의 오너만 존재__ 할 수 있다.

    - 오너가 스코프 밖으로 벗어나는 때, __값은 버려진다(dropped).__

+ 변수의 스코프

    ```rust
    {                      // s는 유효하지 않습니다. 아직 선언이 안됐거든요.
        let s = "hello";   // s는 이 지점부터 유효합니다.

        // s를 가지고 뭔가 합니다.
    }
    // 이 스코프는 이제 끝이므로, s는 더이상 유효하지 않습니다.
    ```
---
+ String 타입

    ```rust
    let mut s = String::from("hello");

    s.push_str(", world!"); // push_str()은 해당 스트링 리터럴을 스트링에 붙여줍니다.

    println!("{}", s); // 이 부분이 `hello, world!`를 출력할 겁니다.
    ```
    > 스트링 리터럴은 프로그램 안에 하드코딩되어 있음.

    > 스트링 리터럴은 편리하지만, 불변이기 때문에 모든 경우에 적절하지는 않음.

    > Rust는 String 문자열 타입을 제공

    > String은 변할 수 있는데 스트링 리터럴은 안될까요? 차이점은 두 타입이 메모리를 쓰는 방식에 있음
---
+ 메모리와 할당

    - 스트링 리터럴은 텍스트가 최종 실행파일에 직접 하드코딩되어 있음.

    - 컴파일 타임에 알 수 없는 크기의 문자열, 실행 중 크기가 변하는 텍스트 조각은 실행파일, 즉 바이너리 안에 하드코딩될 수 없음.

    - String 타입은 변경 가능하고 커질 수 있는 텍스트를 지원

        1. 런타임에 운영체제로부터 __메모리가 요청__ 되어야 한다.
        2. String의 사용이 끝났을 때 운영체제에게 __메모리를 반납할 방법이 필요__ 하다.

        - 첫번째는 String::from을 호출하면, 구현부분에서 필요한 만큼의 메모리를 요청
        - 두번째는 가비지 콜렉터(GC)를 가지고 있다면 프로그래머가 해제할 필요가 없고, __GC가 없다면 프로그래머는 명시적으로 반환코드를 호출__ 해야 함

    - 메모리의 할당과 반환은 역사적으로 프로그램의 안정성과 밀접한 관련이 있음 : 반환를 잊어먹으면? __leackage__, 너무 빨리 반납해버리면? __use after free__, 만일 반납을 두번하면? __double free__
---
+ 변수와 데이터가 상호작용하는 방법: __이동(move)__

    - Stack
        ```rust
        let x = 5;
        let y = x;
        ```
        > 변수 x의 정수값을 y에 대입하기

    - String
        ```rust
        let s1 = String::from("hello");
        let s2 = s1;
        ```
        > 이 코드는 위 변수 x와 y의 동작방식과 전혀 다르다.
---
+ String의 메모리 구조?
1. s1 변수에 "hello"값이 저장된 String의 메모리 구조
2. s1의 포인터, 길이값, 용량값이 복사된 s2 변수의 메모리 구조 (shallow copy)
3. rust 가 힙 데이터까지 복사하게 될 경우 s2 = s1가 만들 또다른 가능성 (deep copy)
![width:1024](string.png)
---
+ String의 메모리 구조

    ```rust
    let s1 = String::from("hello");
    let s2 = s1;

    println!("{}, world!", s1);
    ```

    ```bash
    error[E0382]: use of moved value: `s1`
     --> src/main.rs:4:27
      |
    3 |     let s2 = s1;
      |         -- value moved here
    4 |     println!("{}, world!", s1);
      |                            ^^ value used here after move
      |
      = note: move occurs because `s1` has type `std::string::String`,
    which does not implement the `Copy` trait
    ```
    > s1을 s2로 대입 후, s1을 참조하는 경우 __value used here after move__ error 발생
---
+ __이동(move) : s1이 무효화된 후의 메모리 구조__

![width:450](move.png)
---
> s2만 유효한 상황, 문자열 prt에 대한 소유권이 s1에서 s2로 __이동(move)__
---
+ 변수와 데이터가 상호작용하는 방법: 클론(clone)
    deep copy를 원한다면 clone을 사용. clone에 대한 내용은 다음에...
    ```rust
    let s1 = String::from("hello");
    let s2 = s1.clone();

    println!("s1 = {}, s2 = {}", s1, s2);
    ```
+ 스택에만 있는 데이터: 복사(Copy)
    ```rust
    let x = 5;
    let y = x;

    println!("x = {}, y = {}", x, y);
    ```
    > 스택에 저장할 수 있는 타입은 Copy 트레잇이라고 불리우는 특별한 어노테이션(annotation)을 가짐
    > Copy가 가능한 몇가지 타입 : u32와 같은 모든 정수형 타입들, true와 false값을 갖는 부울린 타입 bool, f64와 같은 모든 부동 소수점 타입들, Copy가 가능한 타입만으로 구성된 튜플들. (i32, i32)는 Copy가 되지만, (i32, String)은 안됨
---
# 소유권과 함수
---
+ 소유권과 스코프에 대한 설명이 주석으로 달린 함수들
    ```rust
    fn main() {
        let s = String::from("hello"); // s가 스코프 안으로 들어왔습니다.

        takes_ownership(s); // s의 값이 함수 안으로 이동했습니다.
                            // 그리고 이제 더이상 유효하지 않습니다.

        let x = 5;     // x가 스코프 안으로 들어왔습니다.

        makes_copy(x); // x가 함수 안으로 이동했습니다만,
                       // i32는 Copy가 되므로, x를 이후에 계속 사용해도 됩니다.

    } // 여기서 x는 스코프 밖으로 나가고,
      // s도 그 후 나갑니다. 하지만 s는 이미 이동되었으므로, 별다른 일이 발생하지 않습니다.

    fn takes_ownership(some_string: String) { // some_string이 스코프 안으로 들어왔습니다.
        println!("{}", some_string);
    } // 여기서 some_string이 스코프 밖으로 벗어났고 `drop`이 호출됩니다.
      // 메모리는 해제되었습니다.

    fn makes_copy(some_integer: i32) {
        // some_integer이 스코프 안으로 들어왔습니다.
        println!("{}", some_integer);
    } // 여기서 some_integer가 스코프 밖으로 벗어났습니다. 별다른 일은 발생하지 않습니다.
    ```
---
+ 참조자(References)와 빌림(Borrowing)

    ```rust
    fn main() {
        let s1 = String::from("hello");
        let len = calculate_length(&s1);
        println!("The length of '{}' is {}.", s1, len);
    }

    fn calculate_length(s: &String) -> usize { // s는 String의 참조자입니다
        s.len()
    } // 여기서 s는 스코프 밖으로 벗어났습니다. 하지만 가리키고 있는 값에 대한 소유권이 없기
      // 때문에, 아무런 일도 발생하지 않습니다.
    ```
    > calculate_length 함수에 &s1를 넘기고, 함수의 정의 부분에는 String이 아니라 &String을 이용
    > 함수의 파라미터로 __참조자(References)__ 를 만드는 것을 __빌림(Borrowing)__ 이라고 부름
---
+ String s1을 가리키고 있는 &String s

![width:1024](reference.png)
---
---
+ 빌린 값을 고치려 해보기

    ```rust
    fn main() {
        let s = String::from("hello");

        change(&s);
    }

    fn change(some_string: &String) {
        some_string.push_str(", world");
    }
    ```
    ```bash
    error: cannot borrow immutable borrowed content `*some_string` as mutable
    --> error.rs:8:5
      |
    8 |     some_string.push_str(", world");
      |     ^^^^^^^^^^^
    ```
    > 변수가 기본적으로 불변인 것처럼, 참조자도 마찬가지입니다. 참조하는 어떤 것을 변경하는 것은 허용되지 않음
---
+ 가변 참조자(Mutable References)

    ```rust
    fn main() {
        let mut s = String::from("hello");

        change(&mut s);
    }

    fn change(some_string: &mut String) {
        some_string.push_str(", world");
    }
    ```
    > s를 mut로 바꿔고, &mut s로 가변 참조자를 생성하고 some_string: &mut String으로 이 가변 참조자를 받아야 함
    > 가변 참조자는 딱 한가지 큰 제한 : __특정한 스코프 내에 특정한 데이터 조각에 대한 가변 참조자를 딱 하나만__ 만들 수 있다
---
+ 가변 참조자(Mutable References) 제한 (1)

    ```rust
    fn main() {
        let mut s = String::from("hello");
        let r1 = &mut s;
        let r2 = &mut s;
        change(r1);
    }

    fn change(some_string: &mut String) {
        some_string.push_str(", world");
    }
    ```
    ```bash
    error[E0499]: cannot borrow `s` as mutable more than once at a time
      --> src/main.rs:24:14
       |
    23 |     let r1 = &mut s;
       |              ------- first mutable borrow occurs here
    24 |     let r2 = &mut s;
       |              ^^^^^^^ second mutable borrow occurs here
    25 |     let len = calculate_length_with_ref(r1);
       |                                         -- first borrow later used here
    ```
---
+ 가변 참조자(Mutable References) 제한 (2)

    ```rust
    fn main() {
        let mut s = String::from("hello");
        let r1 = &mut s;
        let r2 = &mut s;
        change(r2);
        println!("r2 is '{}'", r2);
    }

    fn change(some_string: &mut String) {
        some_string.push_str(", world");
    }
    ```
    ```bash
    warning: unused variable: `r1`
     --> borrow_twice_ok.rs:3:9
      |
    3 |     let r1 = &mut s;
      |         ^^ help: if this is intentional, prefix it with an underscore: `_r1`
      |
      = note: `#[warn(unused_variables)]` on by default
    ```
    > println!("r2 is '{}'", r2);  다음 라인에 println!("r1 is '{}'", r1);을 추가한다면?
---
+  데이터 레이스(data race)

    - 가변 참조자(Mutable References) 제한은 컴파일 타임에 __데이터 레이스(data race)를 방지하기 위함__

    - 데이터 레이스는 아래에 정리된 세 가지 동작이 발생했을때 나타나는 특정한 레이스 조건
        1. 두 개 이상의 포인터가 동시에 같은 데이터에 접근한다.
        2. 그 중 적어도 하나의 포인터가 데이터를 쓴다.
        3. 데이터에 접근하는데 동기화를 하는 어떠한 메커니즘도 없다.

    - 러스트는 데이터 레이스가 발생할 수 있는 코드가 __컴파일 조차 안되기 때문에 이 문제의 발생을 막아버림__

    - 대부분의 언어들은 참조를 통한 값을 변형하도록 해주기 때문에 RUST를 학습할 때 어려운 부분으로 인식됨
---
+ 여러 개의 가변 참조자(with scope)와 불변 참조자

    ```rust
    let mut s = String::from("hello");

    {
        let r1 = &mut s;
    } // 여기서 r1은 스코프 밖으로 벗어났으므로,
      // 우리는 아무 문제 없이 새로운 참조자를 만들 수 있습니다.
    let r2 = &mut s;
    ```
    ```rust
    fn main() {
        let mut s = String::from("hello");
        let r1 = &s;
        let r2 = &s;
        let r3 = &mut s;
        println!("r1 is '{}'", r1);
        println!("r3 is '{}'", r3); //error 발생
    }
    ```
---
+ 가변 참조자 사용의 다른 예

    ```rust
    fn main() {
        let mut s = String::from("hello");
        let r1 = &s;
        let r2 = &s;
        let r3 = &mut s;
        println!("r3 is '{}'", r3);
    }
    ```
    ```rust
    fn main() {
        let mut s = String::from("hello");
        let r1 = &mut s;
        let r2 = &mut s;
        println!("r2 is '{}'", r2);
        println!("r1 is '{}'", r1);
    }
    ```
    > println!("r1 is '{}'", r1);를 제거하면?
---
+ 댕글링 참조자(Dangling References)

    ```rust
    fn main() {
        let reference_to_nothing = dangle();
    }

    fn dangle() -> &String { // dangle은 String의 참조자를 반환합니다
        let s = String::from("hello");

        &s // 우리는 String s의 참조자를 반환합니다.
    } // 여기서 s는 스코프를 벗어나고 버려집니다. 이것의 메모리는 사라집니다.
      // 위험하군요!
    ```
    ```bash
    error[E0106]: missing lifetime specifier
     --> dangle.rs:5:16
      |
    5 | fn dangle() -> &String {
      |                ^^^^^^^
      |
      = help: this function's return type contains a borrowed value, but there is no
        value for it to be borrowed from
      = help: consider giving it a 'static lifetime

    error: aborting due to previous error
    ```
---
+ 댕글링 참조자(Dangling References) 해법

    ```rust
    fn no_dangle() -> String {
        let s = String::from("hello");

        s
    }
    ```
    > 코드는 아무런 문제없이 동작함. 소유권이 밖으로 이동되었고, 아무것도 할당 해제되지 않음

+ __참조자의 규칙__

    1. 어떠한 경우이든 간에, __아래 둘 다는 아니고 둘 중 하나만 가질 수 있도록__ 프로그래밍하는 것이 중요

        - 하나의 가변 참조자
        - 임의 개수의 불변 참조자들

    2. 참조자는 __항상 유효__ 해야만 한다.
---
+ first_word 함수

    ```rust
    fn main() {
        let mut s = String::from("hello world");
        let word = first_word(&s); // word는 5를 갖게 될 것입니다.

        s.clear();// 이 코드는 String을 비워서 ""로 만들게 됩니다.

        // word는 여기서 여전히 5를 갖고 있지만, 5라는 값을 의미있게 쓸 수 있는 스트링은 이제 없습니다.
        // word는 이제 완전 유효하지 않습니다!
    }

    fn first_word(s: &String) -> usize {
        let bytes = s.as_bytes(); // as_bytes 메소드를 이용하여 바이트 배열로 변환

        // iter 메소드를 이용하여 바이트 배열의 반복자(iterator)를 생성
        // enumerate 메소드가 튜플을 반환, 튜플을 해체하기 위해 패턴을 이용
        for (i, &item) in bytes.iter().enumerate() {
            if item == b' ' { // 바이트 리터럴 문법을 이용하여 공백 문자를 나타내는 바이트를 찾음
                return i;
            }
        }
        s.len() //반환을 위한 표현식
    }
    ```
---
+ 슬라이스(Slices) : 소유권을 갖지 않는 또다른 데이터 타입

    ```rust
    let s = String::from("hello world");
    let hello = &s[0..5];
    let world = &s[6..11];
    ```
    > 스트링 슬라이스 : [0..5]라는 코드는 String의 일부분에 대한 참조자, __start..end 문법은 start부터 시작하여 end를 포함하지 않는 연속된 범위__

![width:420](slice.png)
---
---
+ 러스트의 .. 범위 문법

    ```rust
    let s = String::from("hello");
    let len = s.len();

    // 두 줄은 동일한 표현
    let slice = &s[0..2];
    let slice = &s[..2];

    /*-----------------------------------*/
    //두 줄은 동일한 표현
    let slice = &s[3..len];
    let slice = &s[3..];

    /*-----------------------------------*/
    //두 줄은 동일한 표현
    let slice = &s[0..len];
    let slice = &s[..];
    ```
---
+ 슬라이스를 반환하는 first_word 함수

    ```rust
    fn first_word(s: &String) -> &str {
        let bytes = s.as_bytes();

        for (i, &item) in bytes.iter().enumerate() {
            if item == b' ' {
                return &s[0..i];
            }
        }

        &s[..]
    }
    ```
    > __"스트링 슬라이스”__ 를 나타내는 타입은 __&str__
    > first_word가 호출되면, 해당 데이터와 묶여있는 하나의 값을 반환, 이 값은 슬라이스의 시작 위치에 대한 참조자와 슬라이스의 요소 개수로 이루어져 있음
---
+ 빌림 규칙 위반

    ```rust
    fn main() {
        let mut s = String::from("hello world");
        let word = first_word(&s);

        s.clear(); // Error!

        println!("the first word is: {}", word);
    }
    ```
    ```bash
       |
    15 |     let word = first_word(&s);
       |                           -- immutable borrow occurs here
    16 |
    17 |     s.clear(); // Error!
       |     ^^^^^^^^^ mutable borrow occurs here
    18 |
    19 |     println!("the first word is: {}", word);
       |                                       ---- immutable borrow later used here
    ```
---
+ 스트링 리터럴은 슬라이스

    ```rust
    fn first_word(s: &str) -> &str {
        let bytes = s.as_bytes();

        for (i, &item) in bytes.iter().enumerate() {
            if item == b' ' {
                return &s[0..i];
            }
        }
        &s[..]
    }

    fn main() {
        let my_string = String::from("hello world");

        // first_word가 `String`의 슬라이스로 동작합니다.
        let word = first_word(&my_string[..]);

        let my_string_literal = "hello world";

        // first_word가 스트링 리터럴의 슬라이스로 동작합니다.
        let word = first_word(&my_string_literal[..]);

        // 스트링 리터럴은 *또한* 스트링 슬라이스이기 때문에,
        // 아래 코드도 슬라이스 문법 없이 동작합니다!
        let word = first_word(my_string_literal);
    }
    ```
    > first_word 함수의 시그니처가 fn first_word(s: __&str__) -> &str 으로 변경
---
+ 그 밖의 슬라이스들

    ```rust
    let a = [1, 2, 3, 4, 5];
    let a_slice = &a[1..3];
    ```
    > a_slice는 &[i32] type의 slice, 스트링 슬라이스가 동작하는 방법과 같음

+ 정리
    1. 소유권, 빌림, 그리고 슬라이스의 개념은 러스트 프로그램의 메모리 안정성을 컴파일 타임에 보장함
    2. 소유권은 러스트의 다른 수많은 부분이 어떻게 동작하는지에 영향을 줌

+ 개인적인 생각
    1. 소유권은 기존 언어에는 없었던 차별적인 특성.
    2. Rust를 사용하여 구현시 __처음에는 어려운 점__ 이 있겠지만 __명확한 규칙과 제한__ 으로 메모리 할당과 해제에 대한 __모호함__ 이 사라질 것 같음
    3. 메모리로 발생하는 문제가 __획기적으로 줄어들지 않을까?__
