---
marp: true
paginate: false
backgroundColor: white
---

<style>
h1 {
  font-size:  1.9rem;
}

h2 {
  color: #8b0000;
}

section.lead h1 {
  text-align: center;
}

section.lead h2 {
  text-align: center;
}
</style>

<!--
theme: default
class: lead
paginate: true
-->

# _The Rust Programming Language 2_

---
# 연관된 데이터들을 구조체로 다루기
---
+ 구조체(struct) 정의와 생성

    ```rust
    struct User {
        username: String,
        email: String,
        sign_in_count: u64,
        active: bool,
    }
    ```
    > 사용자 계정정보를 저장하는 User 구조체 정의
    ```rust
    let user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };
    ```
    > 구조체 User의 인스턴스 생성하기
---
+ 구조체(struct) 생성

    ```rust
     let mut user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };
    user1.email = String::from("anotheremail@example.com");
    ```
    > User 인스턴스의 email필드 변경하기
    ```rust
    fn build_user(email: String, username: String) -> User {
        User {
            email: email,
            username: username,
            active: true,
            sign_in_count: 1,
        }
    }
    ```
    > 사용자의 이메일, 이름을 받아 User구조체의 인스턴스를 반환하는 build_user 함수
---
+ 간단한 필드 초기화

    ```rust
    fn build_user(email: String, username: String) -> User {
        User {
            email,
            username,
            active: true,
            sign_in_count: 1,
        }
    }
    ```
    > 매개변수 email과 username가 구조체의 필드와 이름이 같아, 함수 내에서 특별히 명시하지 않고 초기화 가능

    > email 필드와 email 매개 변수 이름이 같기 때문에 email:email 대신 email만 작성
---
+ 기존 구조체 인스턴스로 새 구조체 인스턴스 생성하기

    ```rust
    let user2 = User {
        email: String::from("another@example.com"),
        username: String::from("anotherusername567"),
        active: user1.active,
        sign_in_count: user1.sign_in_count,
    };

    let user2 = User {
        email: String::from("another@example.com"),
        username: String::from("anotherusername567"),
        ..user1
    };
    ```
    > 새 User 구조체 생성 시 email과 username 필드에는 새 값을 할당하고, 나머지 필드는 user1에서 재사용
    > ..user1 뒤에는 , 가 올 수 없음 : .. 구문은 마지막에 사용
---
+ 튜플 구조체 : 이름이 없고 필드마다 타입은 다르게 정의 가능

    ```rust
    struct Color(i32, i32, i32);
    struct Point(i32, i32, i32);
    struct MyTupleStruct(i32, String);

    let black = Color(0, 1, 2);
    let origin = Point(0, 1, 2);
    println!("{}",  black.1);

    let user = MyTupleStruct(23, String::from("hong gil dong"));
    let MyTupleStruct(age, name) = user;
    println!("age : {}, name : {}", age, name);
    ```
    > Color 와 Point는 다른 type
    > MyTupleStruct는 i32, String type의 필드들로 구성됨.
    > 튜플과 유사함, 이름을 가진 튜플이라고 할 수 있을라나...
---
+ 구조체 데이터의 소유권(Ownership)

    ```rust
    struct User {
        username: &str,
    }

    fn main() {
        let user1 = User {
            email: "someone@example.com",
        };
    }
    ```
    ```bash
    error[E0106]: missing lifetime specifier
     -->
      |
    2 |     username: &str,
      |               ^ expected lifetime parameter
    ```
---
+ 구조체 데이터의 소유권(Ownership)

    ```rust
    #[derive(Debug)]
    struct User<'a> {
        username: &'a str,
        email: &'a str,
        sign_in_count: u64,
        active: bool,
    }

    fn main() {
        let user1 = User {
            email: "someone@example.com",
            username: "someusername123",
            active: true,
            sign_in_count: 1,
        };
        println!("{:?}", user1);
    }
    ```
    > 'a 로 라이프타임(lifetime)을 명시적으로 알려줌 : User와 username, email은 같은 lifetime을 가짐, lifetime은 차후 설명
---
+ 구조체를 이용한 예제 프로그램 (1)

    ```rust
    fn main() {
        let length1 = 50;
        let width1 = 30;

        println!(
            "The area of the rectangle is {} square pixels.",
            area(length1, width1)
        );
    }

    fn area(length: u32, width: u32) -> u32 {
        length * width
    }
    ```
    ```bash
    The area of the rectangle is 1500 square pixels.
    ```
---
+ 구조체를 이용한 예제 프로그램 (2) : 튜플을 이용한 리팩터링

    ```rust
    fn main() {
        let rect1 = (50, 30);

        println!(
            "The area of the rectangle is {} square pixels.",
            area(rect1)
        );
    }

    fn area(dimensions: (u32, u32)) -> u32 {
        dimensions.0 * dimensions.1
    }
    ```
---
+ 구조체를 이용한 예제 프로그램 (2) : 구조체를 이용한 리팩터링, 의미를 더 추가하기

    ```rust
    struct Rectangle {
        length: u32,
        width: u32,
    }

    fn main() {
        let rect1 = Rectangle { length: 50, width: 30 };

        println!(
            "The area of the rectangle is {} square pixels.",
            area(&rect1)
        );
    }

    fn area(rectangle: &Rectangle) -> u32 {
        rectangle.length * rectangle.width
    }
    ```
---
+ 파생 트레잇(derived trait)으로 유용한 기능 추가하기

    ```rust
    ```rust
    #[derive(Debug)]
    struct Rectangle {
        length: u32,
        width: u32,
    }

    fn main() {
        let rect1 = Rectangle { length: 50, width: 30 };

        println!("rect1 is {:?}", rect1);
    }
    ```
    > Debug 트레잇을 파생시키기 위한 어노테이션의 추가 및 디버그 포맷팅을 이용한 Rectangle 인스턴스의 출력

    > __#[derive(Debug)]__ 어노테이션이 없으면 error 발생
---
+ 메소드(method) : 함수와 유사

    ```rust
    #[derive(Debug)]
    struct Rectangle {
        length: u32,
        width: u32,
    }

    // Method는 impl (구현: implementation) 블록 시작
    impl Rectangle {
        // 첫번째 파라미터는 self의 타입이 Rectangle
        // &mut self도 가능 : object도 mutable이어야 함.
        fn area(&self) -> u32 {
            self.length * self.width
        }
    }

    fn main() {
        let rect1 = Rectangle { length: 50, width: 30 };

        println!(
            "The area of the rectangle is {} square pixels.",
            rect1.area()
        );
    }
    ```
---
+ 더 많은 파라미터를 가진 메소드

    ```rust
    impl Rectangle {
        fn area(&self) -> u32 {
            self.length * self.width
        }

        fn can_hold(&self, other: &Rectangle) -> bool {
            self.length > other.length && self.width > other.width
        }
    }

    fn main() {
        let rect1 = Rectangle { length: 50, width: 30 };
        let rect2 = Rectangle { length: 40, width: 10 };
        let rect3 = Rectangle { length: 45, width: 60 };

        println!("Can rect1 hold rect2? {}", rect1.can_hold(&rect2));
        println!("Can rect1 hold rect3? {}", rect1.can_hold(&rect3));
    }
    ```
---
+ 연관함수 : C++의 static method와 비슷

    ```rust
    impl Rectangle {
        fn square(size: u32) -> Rectangle {
            Rectangle { length: size, width: size }
        }
    }
    ```
    > String::from은 연관함수
    > 연관 함수는 새로운 구조체의 인스턴스를 반환해주는 생성자로서 자주 사용
    > 연관 함수는 :: 문법을 이용
    > :: 문법은 연관 함수와 모듈에 의해 생성된 이름공간 두 곳 모두에서 사용

+ 구조체 정리
    - 커스텀 타입을 만들수 있음
    - 러스트의 열거형 기능은 또 다른 커스텀 타입을 생성하는 방법
---
# 열거형과 패턴 매칭
---
+ 열거형 정의하기

    ```rust
    #![allow(unused)]
    fn main() {
        enum IpAddrKind { //열거형.
            V4,
            V6,
        }

        struct IpAddr {
            kind: IpAddrKind,
            address: String,
        }

        let home = IpAddr {
            kind: IpAddrKind::V4,
            address: String::from("127.0.0.1"),
        };

        let loopback = IpAddr {
            kind: IpAddrKind::V6,
            address: String::from("::1"),
        };
    }
    ```
---
+ 열거형 variant에 데이터 바인딩

    ```rust
    #![allow(unused)]
    fn main() {
        enum IpAddr {
            V4(u8, u8, u8, u8),
            V6(String),
        }
        let home = IpAddr::V4(127, 0, 0, 1);
        let loopback = IpAddr::V6(String::from("::1"));
    }
    ```
    ```rust
    enum Message {
        Quit,
        Move { x: i32, y: i32 },
        Write(String),
        ChangeColor(i32, i32, i32),
    }
    ```
    > Quit: 연관된 데이터 없음, Move: 익명 구조체, Write: 하나의 String, ChangeColor: 세 개의 i32
---
+ 열거형의 메소더 정의와 데이터를 바인딩하는 패턴

    ```rust
    #![allow(unused)]
    fn main() {
        enum Message {
            Quit,
            Move { x: i32, y: i32 },
            Write(String),
            ChangeColor(i32, i32, i32),
        }

        impl Message {
            fn call(&self) {
                match *self {  // 메소드 내용은 여기 정의할 수 있습니다.
                    Message::Write(msg) => {
                        println!("message : {}", msg);
                    },
                    _ => { // defalut는 _ 자리지정자로
                        println!("default");
                    }
                }
            }
        }
        let m = Message::Write(String::from("hello"));
        m.call();
    }
    ```
---
+ Option 열거형

    ```rust
    enum Option<T> {
        Some(T),
        None,
    }

    let some_number = Some(5);
    let some_string = Some("a string");

    let absent_number: Option<i32> = None;
    ```
    > <T> 는 러스트의 제네릭(generic) 문법
    > None을 이용하면 러스트에서 Option<T> 열거자의 type을 알려줘야 함
    > Some은 어떤 유효한 값이 바인딩되어 있다는 것을 알 수 있음
    > __None은 어떠한 값도 바인딩되어 있지 않음__
---
+ Option 열거형을 Null 값 대신 사용할 때의 장점 (1)

    - 러스트는 다른 언어들에서 흔하게 볼 수 있는 null이 없음
    - null을 지원하는 언어에서 변수는 항상 'null' 이거나 'not null'인 두 가지 상태 중 하나
    - null인 변수를 not null 변수로 사용하면서 에러가 발생
    - null 을 고안한 Tony Hoare는 null 참조를 10 억 달러의 실수라고 함
    - 그렇다면, null을 사용한 것보다 Option<T>를 사용하는 것은 어떤 장점이?
    - __Option<T> 와 T 는 다른 타입, 컴파일러는 유효한 값이 명확하게 존재할 때는 Option<T> 값 사용을 허락하지 않음.
    - 무슨 소리인가? 코드로 좀 더 알아보자
---
+ Option 열거형을 Null 값 대신 사용할 때의 장점 (2)

    ```rust
    let x: i8 = 5;
    let y: Option<i8> = Some(5);
    let sum = x + y;
    ```
    ```bash
    error[E0277]: the trait bound `i8: std::ops::Add<std::option::Option<i8>>` is
    not satisfied
     -->
      |
    7 | let sum = x + y;
      |           ^^^^^
      |
    ```
    > i8과 Option<i8>은 다른 타입이기 때문에 컴파일 에러 발생
    > x는 i8 type의 명확한 값을 가지고 있지만, y는 Option<i8> type으로 어떠한 값도 바인딩되지 않는 None을 가질 수 있음
    > 따라서 Option<i8>을 i8로 번환해야 하는데 None인 경우에는 변환할 수 없으므로 에러가 발생
---
+ Option 열거형을 Null 값 대신 사용할 때의 장점 (3)

    - __어떤 값이 null 값을 가지려면 Option<T>를 사용해야 하고, null 값이 가진 경우(어떠한 값도 바인딩되지 않는 경우, None)를 반드시 명시적으로 처리해야 함__
    - 이런 제약으로 인해 컴파일 타임에 null 예외처리을 확인하고 null 값의 확산을 제한하고 코드의 안정성을 높일 수 있음 : 의도적으로 결정한 디자인
    - Option<T> 타입을 사용하는 경우 Some 값으로부터 T 값을 어떻게 알아낼 수 있을까?
    - 이전에 언급된 코드에서 살펴 보았듯이 match 흐름 제어 연산자를 사용함
---
+ match 흐름 제어 연산자

    ```rust
    enum Coin {
        Penny,
        Nickel,
        Dime,
        Quarter,
    }

    fn value_in_cents(coin: Coin) -> u32 {
        match coin {
            Coin::Penny => 1,
            Coin::Nickel => 5,
            Coin::Dime => 10,
            Coin::Quarter => 25,
        }
    }

    fn print_is_lucky_penny(coin: Coin) {
        match coin {
            Coin::Penny => {
                println!("행운의 페니!");
            }
            _ => {
                println!("OMG~~");
            }
        }
    }
    ```
    > 기본적인 enum type과 각 값에 해당하는 패턴을 match 표현식으로 구현
---
+ match 흐름 제어 연산자 : 값들을 바인딩하는 패턴들

    ```rust
    #[derive(Debug)] // So we can inspect the state in a minute
    enum UsState {
        Alabama,
        Alaska,
        // ... etc
    }

    enum Coin {
        Penny,
        Nickel,
        Dime,
        Quarter(UsState),
    }

    fn value_in_cents(coin: Coin) -> u32 {
        match coin {
            Coin::Penny => 1,
            Coin::Nickel => 5,
            Coin::Dime => 10,
            Coin::Quarter(state) => {
                println!("State quarter from {:?}!", state);
                25
            },
        }
    }
    ```
---
+ Option<T>를 이용하는 매칭

    ```rust
    fn plus_one(x: Option<i32>) -> Option<i32> {
        match x {
            None => None,
            Some(i) => Some(i + 1),
        }
    }

    let five = Some(5);
    let six = plus_one(five);
    let none = plus_one(None);
    ```
---
+ match는 반드시 모든 경우를 처리해야 함!
+ 자리 지정자(placeholder) _

    ```rust
    let some_u8_value = 0u8;
    match some_u8_value {
        1 => println!("one"),
        3 => println!("three"),
        5 => println!("five"),
        7 => println!("seven"),
        _ => (),
    }
    ```
    > _ 패턴은 모든 값에 일치함을 의미 : switch/case문의 default?
    > match 표현식의 가장 마지막에 추가
    > () : 어떤 동작도 하지 않음, python의 pass?
- __단 한가지 값만 처리해야 하는 경우__ match 표현식은 비효율적임, 러스트는 이런 경우를 대비해 __if let 구문__ 을 제공
---
+ if let을 이용한 간결한 흐름 제어

    ```rust
    fn main() {
        let a = Some(3);

        if let Some(3) = a {
            println!("three");
        }

        if let None = a {
            println!("none");
        }

        test_if_let(a);
        test_if_let(None);
    }

    fn test_if_let(o: Option<i32>) {
        if let Some(value) = o {
            println!("Option<i32>::Some({})", value);
        } else {
            println!("Optoin<i32>::None");
        }
    }
    ```
---
# 패키지, 크레이트(Crate), 모듈
---

+ 범위(scope) : 일종의 중첩된 context, 특정 scope 안에서 작성된 코드는 그 scope내에서만 유효

+ 모듈 시스템 : 러서트가 코드의 구조를 관리하기 위해 제공하는 몇가지 기능

    + 패키지 : 크레이트를 빌드, 테스트, 공유할 수 있는 Cargo의 기능

    + 크레이트(Crate) : 라이브러리나 실행 파일을 생성하는 모듈의 트리(tree)

    + 모듈 use : 코드의 구조와 범위, 그리고 경로의 접근성을 제어하는 기능

    + 경로(path) : 구조체, 함수, 혹은 모듈등의 이름을 결정하는 방식
---
+ 패키지와 크레이트(Crate) (1)
    - 패키지: 하나 이상의 크레이트와 그것을 빌드하는 방법을 서술하는 Cargo.toml 파일로 구성
    - 크레이트(Crate):  하나의 바이너리 혹은 라이브러리로서, 모듈로 구성
    - 크레이트 루트(Crate root): 러스트 컴파일러가 컴파일을 시작하는 소스파일

        ```bash
        dal@MCRND1R8:~/projects/rust/seminar/rust/src$ cargo new my-project
            Created binary (application) `my-project` package
        dal@MCRND1R8:~/projects/rust/seminar/rust/src$ tree my-project/
        my-project/
        ├── Cargo.toml
        └── src
            └── main.rs
        ```
        > cargo new <packate-name> 으로 새로운 패키지를 생성
        > src/main.rs 는 패키지와 같은 이름을 갖는 바이너리 크레이트(crate)의 크레이트 루트(crate root)
---
+ 패키지와 크레이트(Crate) (2)

    + Cargo.toml : 러스트가 크레이트를 빌드하는 방법을 서술한 파일

        ```ini
        [package]
        name = "my-project"
        version = "0.1.0"
        edition = "2018"

        # See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

        [dependencies]
        ```
        1. package name : "my-project"
        2. crate root인 src/main.js에 대한 기술이 Cargo.toml 없지만 러스트는 명시하지 않아도 바이너리 crate 인 경우에는 src/main.rs 가 crate root라는 규칙을 알고 있음
        3. 라이브러리 crate의 경우에는 src/lib.rs 파일을 crate root로 인식
---
+ 모듈(Module)과 범위(Scope) : 크레이트의 코드는 모듈 단위로 그룹화

    ```bash
    dal@MCRND1R8:~/projects/rust$ cargo new --lib restaurant
        Created library `restaurant` package
    dal@MCRND1R8:~/projects/rust/restaurant$ ls src/lib.rs
    src/lib.rs
    ```
    > restaurant library를 생성해 놓자


+ 모듈(module)
    1. crate의 코드를 그룹화하여 가독성과 재사용성을 높임
    2. 외부의 코드가 크레이트의 코드를 사용할 수 있는지(public), 외부의 코드가 사용할 없는(private) 구현인지를 결정
---
+ 모듈(module)

    ```rust
    src/lib.rs

    mod front_of_house {
        mod hosting {
            fn add_to_waitlist() {}
            fn seat_at_table() {}
        }
        mod serving {
            fn take_order() {}
            fn serve_order() {}
            fn take_payment() {}
        }
    }
    ```
    > __mod 키워드__ 로 module name과 함께 모듈을 정의
    > module의 본문은 중괄호({})를 사용해서 감쌈
    > module에는 구조체, 열거자, 상수, 트레이트, 함수등을 추가할 수 있음
    > 3개의 모듈이 정의됨 : front_of_house, hosting, serving
---
+ restaurant crate의 module tree

    > src/lib.rs와 src/main.rs 파일을 crate root라고 했음. 그 이유는 두 파일은 crate라는 이름의 모듈로 구성되며, 모듈은 module tree라고 부르는 crated의 모듈 구조에서 root 역활을 하기 때문
    ```bash
    crate
    └── front_of_house
        ├── hosting
        │   ├── add_to_waitlist
        │   └── seat_at_table
        └── serving
            ├── table_order
            ├── serve_order
            └── take_payment
    ```
    > 위 module tree를 보면 모듈은 다른 모듈을 포함할 수 있다는 것을 알 수 있음. (일부 모듈이 다른 모듈에 중첩됨)
    > C++의 namespace와 비슷한듯?
---
+ 경로를 이용한 module tree의 내용 참조하기

    - 러스트가 module tree안에서 아이템을 찾을 수 있게 하려면 경로를 이용해야 함

    - 절대 경로 : crate 이름이나 crate 리터럴을 이용해 crate root 부터 시작하는 경로

    - 상대 경로 : 현재 모듈로부터 시작하여 self, super 혹은 현재 모듈의 식별자를 이용

    - 식별자는 이중 콜론(:)으로 구분

    - __add_to_waitlist 함수의 호출 == add_to_waitlist 함수의 경로__

        ```rust
        crate::front_of_house::hosting::add_to_waitlist();
        ```
        > 절대 경로를 사용하는 예
---
+ add_to_waitlist 함수 호출

    ```rust
    mod front_of_house {
        mod hosting {
            fn add_to_waitlist() {}
        }
    }

    pub fn eat_at_restaurant() {
        crate::front_of_house::hosting::add_to_waitlist(); //절대경로
        front_of_house::hosting::add_to_waitlist(); //상대경로
    }
    ```
    ```bash
    $ cargo build
    Compiling restaurant v0.1.0 (/home001/indal.choi/projects/rust/seminar/rust/src/restaurant)
    error[E0603]: module `hosting` is private
      --> src/lib.rs:8:28
      |
    8 |     crate::front_of_house::hosting::add_to_waitlist(); //절대경로
      |                            ^^^^^^^ private module
    error[E0603]: module `hosting` is private
      --> src/lib.rs:9:21
      |
    9 |     front_of_house::hosting::add_to_waitlist(); //상대경로
      |                     ^^^^^^^ private module
    ```
---
+ pub 키워드로 공개하기

    - 러스트에서 접근성이 동작하는 방식은 모든 아이템(함수, 메서드, 구조체, 열거자, 모듈, 그리고 상수등)은 기본적으로 __비공개__
    - 부모 모듈은 자식 모듈 안의 비공개 아이템 사용 불가, 반대는 가능 = 하위 모듈은 비공개 아이템 접근 불가, 상위 모듈은 접근 가능
    - __pub 키워드__ 를 사용하여 부모(상위) 모듈에게 공개할 수 있음
        ```rust
        mod front_of_house {
            pub mod hosting {
                pub fn add_to_waitlist() {}
            }
        }
        ```
        > __접근성 규칙은 아이템에도 적용되기 때문에 add_to_waitlist 함수에도 pub 키워드를 추가해야 함__
---
+ 접근성 규칙 조금 더 살펴보기

    ``` rust
    mod front_of_house {
        pub mod hosting {
            pub fn add_to_waitlist() {}
        }
    }
    pub fn eat_at_restaurant() {
        crate::front_of_house::hosting::add_to_waitlist(); //절대경로
        front_of_house::hosting::add_to_waitlist(); //상대경로
    }
    ```

    - front_of_house 모듈은 pub가 아닌데 eat_at_restaurant 함수에서 접근이 가능하는 이유?

        > 같은 crate 모듈에 정의되어 있기 때문에 두 아이템 front_of_house 와 eat_at_restaurant는 서로 __형제 관계__ (crate::front_of_house, crate::eat_at_restaurant)
        > 형제 관계이므로 eat_at_restaurant 함수는 front_of_house 모듈을 참조할 수 있음.
---
+ super로 시작하는 상대경로

    ```rust
    fn serve_order() {}

    mod back_of_house {
        fn fix_incorrect_order() {
            cook_order();
            super::server_order();
        }
        fn cook_order();
    }
    ```
    > super로 시작하는 상대 경로로 함수 호출
---
+ 구조체와 열거자 공개하기

    ```rust
    mod back_of_house {
        pub struct Breakfast {
            pub toast: String, // 마침표(.) 표기로 접근 가능.
            seasonal_fruit: String, // 마침표(.) 표기로 접근 불가능.
        }

        impl Breakfast {
            // method도 접근성 규칙을 따르기 때문에 pub 키워드를 추가
            pub fn summer(toast: &str) -> Breakfast {
                Breakfast {
                    toast: String::from(toast),
                    seasonal_fruit: String::from("peaches"),
                }
            }
        }

        pub enum Appetizer { //열거자를 공개하면 열거값도 공개됨
            Soup,
            Salad,
        }
    }

    let appetizer = back_of_house::Appetizer::Salad;
    let mut meal = back_of_house::Breakfast::summer("호밀빵");
    meal.toast = String::from("밀빵"); // toast는 pub 이므로 접근가능
    println!('{}  토스트로 주세요.', meal.toast);
    ```
---
+ use 키워드로 경로를 범위로 가져오기

    ```rust
    mod front_of_house {
        pub mod hosting {
            pub fn add_to_waitlist() {}
        }
    }

    use crate::front_of_houst::hosting;

    pub fn eat_at_restaurant() {
        hosting::add_to_waitlist();
    }
    ```
    > use 키워드로 경로를 현재 버위로 가져오면 경로의 아이템이 마치 현재 범위의 아이템인 것처럼 호출 가능
    > use 키워드는 파일 시스템에서 심볼릭 링크(symbolic link)를 생성하는 것과 유사

---
+ use를 이용한 간결한 가져오기

    ```rust
    pub mod a {
        pub mod series {
            pub mod of {
                pub fn nested_modules() {}
            }
        }
    }

    use a::series::of::nested_modules;

    fn main() {
        nested_modules();
    }
    ```
    > 함수를 직접 참조할 수 있도록 use 구문에서 함수를 명시함 : use a::series::of::nested_modules;
---
+ enum type의 variant를 use를 이용하여 가져오기

    ```rust
    enum TrafficLight {
        Red,
        Yellow,
        Green,
    }

    use TrafficLight::{Red, Yellow};

    fn main() {
        let red = Red;
        let yellow = Yellow;
        let green = TrafficLight::Green;
    }
    ```
---
+ 서로 다른 부모 모듈에 정의된 같은 이름

    ```rust
    use std::fmt;
    use std::io;

    fn function1() -> fmt::Result {
    }
    fn function2() -> io::Result<()> {
    }
    ```

    ```rust
    use std::fmt::Result;
    use std::io::Result as IoResult;

    fn function1() -> Result {
    }
    fn function2() -> IoResult<()> {
    }
    ```
    > as 키워드를 사용하여 범위로 가져오는 타입의 이름 재정의

---
+ *를 이용한 모두 가져오기 (glob)

    ```rust
    enum TrafficLight {
        Red,
        Yellow,
        Green,
    }

    use TrafficLight::*;

    fn main() {
        let red = Red;
        let yellow = Yellow;
        let green = Green;
    }
    ```
    > *는 글롭(glob) 이라고 부름 : 이름공간 내에 공개된 모든 아이템을 가져옴
    > 글롭은 편리하지만 이름 간의 충돌(naming conflict)의 원인이 될 수 있음

---
+ pub use 키워드로 이름 다시 내보내기

    ```rust
    mod front_of_houst {
        pub mod hosting {
            pub fn add_to_waitlist() {}
        }
    }

    pub use crate::front_of_house::hosting;

    pub fn eat_at_restaurant() {
        hosting::add_to_waitlist();
        hosting::add_to_waitlist();
        hosting::add_to_waitlist();
    }
    ```
    > pub use 키워드는 현재 범위로 가져온 것을 다시 내보냄.
    > 외부의 코드에서 hosting::add_to_waitlist 구문을 통해 add_to_waitlist 함수 호출 가능

---
+ 외부 패키지의 사용

    ```ini
    [dependencies]
    rand = "0.5.5"
    ```
    > Cargo.toml 파일에 rand라는 외부 패키지의 의존(dependencies)을 추가
    > rand 패키지는 https://crates.io/ 에서 내려받음

    ```rust
    use rand::{thread_rng, Rng};

    fn main() {
        let mut rng = thread_rng();
        let x: u32 = rng.gen();
        println!("{}", x);
        println!("{:?}", rng.gen::<(f64, bool)>());
    }
    ```
    > Cargo.toml에 rand 패키지를 의존에 추가한 후 use 구문을 사용해 thread_rng 함수와 Rng 트레이트를 사용

---
+ 표준 라이브러리(std)도 외부 크레이트

    - 표준 라이브러리는 Cargo.toml 파일의 의존(dependencies)에 추가할 필요없음

    - 표준 라이브러리를 사용하기 위해서는 외부 패키지의 사용과 같이 use 키워드로 범위를 지정해야 함

        ```rust
        use std::collections::HaspMap;
        ```

    - std는 표준 라이브러리의 크레이트 이름

---
+ 중첩 경로(Nested Path)로 use 목록 유지

    ```rust
    use std::io;
    use std::cmp::Ordering;
    ```
    > 중첩 경로는 아래와 같이 사용할 수 있다

    ```rust
    use std::{io, cmp::Ordering};
    ```

+ 완전히 일치하는 경로가 있는 경우
    ```rust
    use std::io;
    use std::io::Write;
    ```
    > 완전히 일치하는 경로가 있다면 그것은 self
    ``` rust
    use std::io::{self, Write};
    ```

---
+ 모듈 분할
    - source files
        ```bash
        ├── back_of_house.rs
        ├── front_of_house
        │   ├── hosting.rs
        │   ├── mod.rs
        │   └── serving.rs
        └── lib.rs
        ```
    - crate
        ```bash
        ├── front_of_house
        │   ├── hosting
        │   │   ├── add_to_waitlist
        │   │   └── seat_at_table
        │   └── serving
        │       ├── table_order
        │       ├── serve_order
        │       └── take_payment
        └── back_of_house
            ├── fix_incorrect_order
            └── cook_order
        ```
---
# 범용 컬렉션 (Common Collections)
# vector, string, hash map
---
+ 벡터 생성하기

    ```rust
    let v: Vec<i32> = Vec::new();
    ```

    > 타입 명시(type annotation)를 사용 : 벡터에 어떠한 값도 저장하지 않았으므로 타입을 명시해야 함
    > rust는 벡터에 값을 추가면 그 타입을 유추함


    ```rust
    lec v = vec![1, 2, 3];
    ```
    > rust는 Vec<i32> 벡터를 생성함.
---
+ 벡터 수정

    ```rust
    let mut v = Vec::new();

    v.push(5);
    v.push(6);
    v.push(7);
    v.push(8);
    ```
    > push 값으로 전달한 모든 값을 i32로 추론 가능
    > rust는 데이터르부터 type을 추론하므로 Vec<i32>로 타입 명시를 할 필요없음

+ 벡터 해제

    ```rust
    {
        let v = vec![1, 2, 3];
    } // <- 변수가 scope를 벗어나면 drop method가 호출되어 메모리가 해제됨.
    ```
---
+ 벡터로부터 값 읽기

    ```rust
    let v = vec![1, 2, 3];

    let third1: &i32 = &v[2];
    println!("(1) Third element is {}", third1);

    let third2: Option<&i32> = v.get(2);
    println!("(2) Third element is {:?}", third2);

    match v.get(2) {
        Some(third3) => println!("(3) Third element : {}", third3),
        None => println!("(3) Third element doesn't exist")
    }
    ```
---
+ 유효하지 않은 참조자

    ```rust
    let mut v = vec![1, 2, 3, 4, 5];

    let first = &v[0];

    v.push(10);

    println!("{}", first);
    ```

    ```bash
       |
    16 |     let first = &v[0];
       |                  - immutable borrow occurs here
    17 |
    18 |     v.push(10);
       |     ^^^^^^^^^^ mutable borrow occurs here
    19 |
    20 |     println!("{}", first);
       |                    ----- immutable borrow later used here
    ```
    > println!("{}", first); 을 주석처리하면??
---
+ 벡터에 저장된 값 순회처리

    ```rust
    let v = vec![1, 2, 3, 4, 5];
    for i in &v {
        println!("{}", i);
    }
    ```
    ```rust
    let mut v = vec![1, 2, 3, 4, 5];
    for i in &mut v {
        *i += 50;
        println!("{}", i);
    }
    ```
    > 가변 참조 순회
---
+ 벡터에 여러 타입 저장하기 : 열거형 사용

    ```rust
    enum SpreadsheetCell {
        Int(i32),
        Float(f64),
        Text(String),
    }

    let row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Text(String::from("blue")),
        SpreadsheetCell::Float(10.12),
    ];
    ```

+ 표준 라이브러리의 Vec에 정의된 수많은 유용한 메소드들은 API 문서 참고
---
+ 새 문자열 생성하기

    ```rust
    let mut s = String::new();
    ```
    > 빈 문자열 생성하기

    ```rust
    let data = "initiail contents";

    let s = data.to_string();

    let s = "initiail contents".to_string();

    let s = String::from("initial contents");
    ```
    > 문자열 리터럴로 문자열 생성하기
---
+ UTF-8 인코딩

    ```rust
    let hello = String::from("السلام عليكم");
    let hello = String::from("Dobrý den");
    let hello = String::from("Hello");
    let hello = String::from("שָׁלוֹם");
    let hello = String::from("नमस्ते");
    let hello = String::from("こんにちは");
    let hello = String::from("안녕하세요");
    let hello = String::from("你好");
    let hello = String::from("Olá");
    let hello = String::from("Здравствуйте");
    let hello = String::from("Hola");
    ```
    > 다양한 언어로 인삿말 저장하기
---
+ 문자열 수정하기

    ```rust
    let mut s = String::from("foo");
    s.push_str("bar");
    ```
    > push_str method로 String에 __문자열 슬라이스(string slice)__ 추가

    ```rust
    let mut s1 = String::from("foo");
    let s2 = "bar";
    s1.push_str(&s2);
    println!("s2 is {}", s2); // push_str은 s2의 소유권을 가져가지 않음
    ```
    > String 타입의 변수에 스트링 슬라이스를 붙인 후 변수 다시 사용

    ```rust
    let mut s = String::from("lo");
    s.push('l');
    ```
    > push를 사용하여 String 값에 한 글자 추가
---
+ +연산자나 formt! 매크로를 이용한 문자열 연결

    ```rust
    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");
    let s3 = s1 + &s2; // s1은 메모리가 해제되어 더 이상 사용 불가
    ```
    > s2는 참조로 전달한 이유는 + 연산자는 내부적으로 add method를 사용

    ```rust
    fn add(self, s: &str) -> String {
    ```
    > &s2는 String 참조, add method의 두 번째 파라미터는 &str로 문자열 슬라이스?
    > 강제 역참조(deref coercion) : &s2를 &s2[..]로 변환
    > 첫 번째 파라미터 self는 소유권이 있음 : add method는 s1의 소유권을 확보하고 s2의 문자열을 추가한 후 결과값의 소유권을 반환함.
---
+ format! 매크로

    ```rust
    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");

    let s = s1 + "-" + &s2 + "-" + &s3;
    ```
    > 여러 스트링을 접하고자 한다면, +의 동작은 다루기 불편

    ```rust
    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");

    let s = format!("{}-{}-{}", s1, s2, s3);
    ```
    > format! macro는 파라미터들의 소유권을 가져가지 않음
---
+ 문자열 내부 인덱스

    ```rust
    let s1 = String::from("hello");
    let h = s1[0];
    ```
    ```bash
       |
    14 |     let h = s1[0];
       |             ^^^^^ `String` cannot be indexed by `{integer}`
       |
    ```

    ```rust
    let len = String::from("Hola").len(); // 4
    let len = String::from("안녕하세요").len(); // 15

    let hello = "안녕하세요";
    let answer = &hello[0]; // compile error
                            //  : string indices are ranges of `usize`
    ```
    > String은 Vec<u8>을 감싼 것
---
+ 문자열 순회하기

    ```rust
    let hello = "안녕하세요";
    for c in hello.chars() {
        println!("{}", c);
    }

    println!("{}", hello.len()/hello.chars().count());

    let an = '안';
    println!("length : {:?}",an.len_utf8());
    ```
    ```rust
    let answer = &hello[0..2];
    println!("{:?}", answer);
    ```
    ```bash
    thread 'main' panicked at 'byte index 2 is not a char boundary; it is inside '안' (bytes 0..3) of `안녕하세요`'
    ```
---
+ 새로운 해시 맵 생성하기

    ```rust
    use std::collections::HashMap;

    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);
    ```
    ```rust
    use std::collections::HashMap;

    let teams  = vec![String::from("Blue"), String::from("Yellow")];
    let initial_scores = vec![10, 50];

    let scores: HashMap<_, _> = teams.iter().zip(initial_scores.iter()).collect();
    ```
    > 해시 맵은 튜플의 벡터에 대해 collect 메소드로도 생성 가능
    > HashMap<_, _> : 벡터에 담긴 데이터의 타입에 기초하여 해쉬에 담길 타입을 추론
---
+ 해시 맵의 소유권

    ```rust
    use std::collections::HashMap;

    let field_name = String::from("Favorite color");
    let field_value = String::from("Blue");

    let mut map = HashMap::new();
    map.insert(field_name, field_value);
    // field_name과 field_value은 이 지점부터 유효하지 않습니다.
    // 이 값들을 사용하려고 하면 컴파일 에러가 발생
    ```
    > 해쉬맵에 값들의 참조자들을 삽입한다면 소유권은 이동하지 않음. 하지만 참조자가 가리키고 있는 값은 해쉬맵이 유효할 때까지 계속 유효해야 함
---
+ 해시 맵의 값 접근

    ```rust
    use std::collections::HashMap;

    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    let team_name = String::from("Blue");
    let score = scores.get(&team_name);

    println!("{:?}", score);
    ```
    ```bash
    Some(10)
    ```
    > __get method는 Option<&V>를 반환__ : match 문 활용
---
+ for 문을 이용한 해시 맵의 값 접근

    ```rust
    use std::collections::HashMap;

    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    for (key, value) in &scores {
        println!("{}: {}", key, value);
    }
    ```
    ```bash
    Blue: 10
    Yellow: 50
    ```
    > key   : &String
    > value : &i32
---
+ 해시 맵 수정하기

    ```rust
    use std::collections::HashMap;

    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Blue"), 25);

    println!("{:?}", scores);
    ```
    > 똑같은 키에 다른 값을 삽입하면 값 덮어쓰기
    > 키에 값이 할당되어 있지 않는 경우에만 추가
---
+ 해시 맵의 기존 값을 사용하여 수정하기

    ```rust
    use std::collections::HashMap;

    let text = "hello world wonderful world";

    let mut map = HashMap::new();

    for word in text.split_whitespace() {
        println!("{:?}", map.entry(word));
        let count = map.entry(word).or_insert(0);
        *count += 1;
    }

    println!("{:?}", map);
    ```
    > map.entry()는 key, value로 구성된 Entry enum type을 반환
    > Entry의 or_insert method는 해당 키에 대한 값의 가변 참조자 (&mut V)를 반환

+ HashMap은 서비스 거부 공격(Denial of Service(DoS) attack)에 저항 기능을 제공할 수 있는 암호학적으로 보안되는 해쉬 함수를 사용
---
# 에러 처리
## 복구 가능한(recoverable) 에러
## 복구 불가능한(unrecoverable) 에러
---
+ panic! 매크로 : 복구 불가능한 에러 처리

    ```rust
    fn main() {
        panic!("crash and burn");
    }
    ```
    ```bash
    $ cargo run
        Finished dev [unoptimized + debuginfo] target(s) in 0.00s
        Running `target/debug/ex_panic`
    thread 'main' panicked at 'crash and burn', src/main.rs:2:5
    note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
    ```
---
+ panic! 백트레이스 사용하기

    ```rust
    fn main() {
        let v = vec![1, 2, 3];
        v[99];
    }
    ```
    ```bash
    $ RUST_BACKTRACE=1 cargo run
   Compiling ex_panic v0.1.0 (/media/dalu/data/projects/rust/seminar/rust/src/ex_panic)
        Finished dev [unoptimized + debuginfo] target(s) in 0.29s
        Running `target/debug/ex_panic`
    thread 'main' panicked at 'index out of bounds: the len is 3 but the index is 99', src/main.rs:4:5
    stack backtrace:
    0: rust_begin_unwind
                at /rustc/a178d0322ce20e33eac124758e837cbd80a6f633/library/std/src/panicking.rs:515:5
    1: core::panicking::panic_fmt
                at /rustc/a178d0322ce20e33eac124758e837cbd80a6f633/library/core/src/panicking.rs:92:14
    2: core::panicking::panic_bounds_check
                at /rustc/a178d0322ce20e33eac124758e837cbd80a6f633/library/core/src/panicking.rs:69:5
    3: <usize as core::slice::index::SliceIndex<[T]>>::index
                at /rustc/a178d0322ce20e33eac124758e837cbd80a6f633/library/core/src/slice/index.rs:184:10
    4: core::slice::index::<impl core::ops::index::Index<I> for [T]>::index
                at /rustc/a178d0322ce20e33eac124758e837cbd80a6f633/library/core/src/slice/index.rs:15:9
    5: <alloc::vec::Vec<T,A> as core::ops::index::Index<I>>::index
                at /rustc/a178d0322ce20e33eac124758e837cbd80a6f633/library/alloc/src/vec/mod.rs:2428:9
    6: ex_panic::main
                at ./src/main.rs:4:5
    7: core::ops::function::FnOnce::call_once
                at /rustc/a178d0322ce20e33eac124758e837cbd80a6f633/library/core/src/ops/function.rs:227:5
    note: Some details are omitted, run with `RUST_BACKTRACE=full` for a verbose backtrace.
    ```
---
+ panic!에 발생했을 때 스택 unwind 그만두기

    1. 기본적으로, panic!이 발생하면, 프로그램은 stack unwinding 를 시작

    2. 러스트는 스택을 역순으로 순회하면서 각 함수에 전달되었던 데이터를 정리

    3. 이 경우 실행되는 작업의 양이 무척 큼

    4. stack unwinding을 하지 않고 프로세스를 종료할 수 있음

    ```ini
    [profile.release]
    panic = 'abort'
    ```
---
+ Result type으로 복구 가능한 에러 처리

    ```rust
    enum Result<T, E> {
        Ok(T),
        Err(E),
    }
    ```
    > T와 E는 제네릭 타입 파라미터 : 제네릭 타입은 다음 세미나에서...
    > T는 Ok variant의 값, E는 Err variant의 값
---
+ File::open이 반환하는 Result type

    ```rust
    use std::fs::File;

    fn main() {
        let f: u32 = File::open("hello.txt");
    }
    ```

    ```bash
    error[E0308]: mismatched types
    --> file_open.rs:4:18
      |
    4 |     let f: u32 = File::open("hello.txt");
      |            ---   ^^^^^^^^^^^^^^^^^^^^^^^ expected `u32`, found enum `Result`
      |            |
      |            expected due to this
      |
      = note: expected type `u32`
                 found enum `Result<File, std::io::Error>`
    ```
    > Result<File, std::io::Error>
    > Ok variant는 File, Err variant는 std::io::Error을 값으로 가짐
    > File == std::fs::File type : file handle
---
+ match를 이용한 Result type 처리

    ```rust
    use std::fs::File;

    fn main() {
        let f = File::open("hello.txt");

        let f = match f {
            Ok(file) => file,
            Err(error) => {
                panic!("There was a problem opening the file: {:?}", error)
            },
        };
    }
    ```